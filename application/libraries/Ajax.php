<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax {

	public function response($controller, string $view, $data = null)
	{
        $controller->load->view($view, $data);
        $string = $controller->output->get_output();
        return $controller->output->set_content_type('text/plain', 'UTF-8')
            ->set_output($string);
	}
}
