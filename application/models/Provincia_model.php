<?php
class Provincia_model extends CI_Model {

    public function getProvincias(){
        $this->db->order_by('provincia');
        $query = $this->db->get('provincia');
        return $query->result();
    }

    public function getProvinciaById($id){
        $this->db->where('id', $id);
        $query = $this->db->get('provincia');
        return $query->result();
    }

    // Al ser una tabla de datos estáticos se obvian el resto de los métodos
}
