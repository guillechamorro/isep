<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Client extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('client_model','client');
        // Se carga el modelo de provincias
        $this->load->model('provincia_model','provincia');
        $this->load->library('Ajax');

    }

    public function index()
    {
        $data = array();
        $data['clients'] = $this->client->getClients();

        // Por cada cliente se busca la provincia relacionada
        foreach ($data['clients'] as $key => $value) {
            $provincia = $data['clients'][$key]->provincia_id ? $this->provincia->getProvinciaById($data['clients'][$key]->provincia_id) : null;
            $provincia = null !== $provincia ? $provincia[0]->provincia : '<i>No definida</i>';
            $data['clients'][$key]->provincia_id = $provincia;
        }
        $view = 'client/client_view';
        $data['view'] = $view;
        $this->load->view('home_view', $data);
    }

    public function form($client_id = null)
    {
        $data = array();
        // Se traen todas las provincias para elegir en el formulario
        $data['provincias'] = $this->provincia->getProvincias();
        if($client_id){
            $data['client'] = $this->client->getClientById($client_id);
        }
        $data['view'] = 'client/client_form_view';
        $this->load->view('home_view', $data);
    }

    public function save($id = null)
    {
        $form_data = array
        (
            'id' => $id,
            'nombres' => $this->input->post('nombres'),
            'apellidos' => $this->input->post('apellidos'),
            'dni' => $this->input->post('dni'),
            'fecha_nac' => $this->input->post('fecha_nac'),
            'email' => $this->input->post('email'),
            'provincia_id' => $this->input->post('provincia_id'),
        );

        // Si el campo provincia_id está vacío se convierte en null
        // para guardarlo en la db
        if (empty($form_data['provincia_id'])) {
            $form_data['provincia_id'] = null;
        }

        // Conversión de formato fecha de d/m/Y a Y-m-d para poder guardarla en la db
        if (!empty($form_data['fecha_nac'])) {
            $fecha = \DateTime::createFromFormat('d/m/Y', $form_data['fecha_nac']);
            $form_data['fecha_nac'] = $fecha->format('Y-m-d');
        }

        if(!$id){
            $send_form = $this->client->createClient($form_data);
        } else {
            // echo "<pre>";print_r($form_data);echo "</pre>";exit;
            $send_form = $this->client->updateClient($form_data);
        }

        if($send_form){
            $this->session->set_flashdata('mensagem', array('success','Cliente guardado exitosamente!'));
            redirect('client');
        }
        else
        {
            $this->session->set_flashdata('mensagem', array('danger','Ops! Datos incorrectos!'));
            redirect('client/form');
        }
    }

    public function delete($id)
    {
        $delete = $this->client->deleteClient($id);
        if($delete){
            $this->session->set_flashdata('mensagem', array('success','Cliente borrado exitosamente!'));
            redirect('client');
        }
        else
        {
            $this->session->set_flashdata('mensagem', array('danger','Ops! Cliente não encontrado!'));
            redirect('client');
        }
    }
}
