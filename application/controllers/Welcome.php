<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    public function __construct()
    {
        parent::__construct();
        $this->load->model('product_model','product');
        $this->load->model('client_model','client');
    }

	public function index()
	{
        // Se toma el path con el número de cliente que hace el pedido
        $path = $this->uri->segment(1) && ($this->uri->segment(1) === 'client') ? true : false;
        $client_id = ($path && $this->uri->segment(2)) ? $this->uri->segment(2) : false;

        $data = array();
        $data['products'] = $this->product->getProducts();
        // Si existe el cliente se trae el modelo correspondiente
        if ($client_id) {
            $data['client'] = $this->client->getClientById($client_id);
        }
        $data['view'] = 'welcome_message';
        $this->load->view('home_view', $data);
	}
}
