<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Provincia extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('provincia_model','provincia');
    }

    public function index()
    {
        $data = array();
        $data['provincias'] = $this->provincia->getClients();
        $this->load->view('provincia/provincia_view', $data);
    }
}
