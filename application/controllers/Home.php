<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use application\controllers\AjaxTrait;

class Home extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->library('Ajax');
    }

	public function index()
	{
        $view = 'home_dasboard';
        $data['view'] = $view;
        $this->load->view('home_view', $data);
	}
}
