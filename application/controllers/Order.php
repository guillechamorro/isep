<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('product_model','product');
        $this->load->model('order_model','order');
        // Se carga el modelo de cliente
        $this->load->model('client_model','client');
        $this->load->library('Ajax');

    }

    public function index($client_id = null){
        $data = array();
        $data['orders'] = $this->order->getOrders();
        $view = 'order/order_view';
        $data['view'] = $view;
        $this->load->view('home_view', $data);
    }

    public function form($order_id = null)
    {
        $data = array();
        $data['orders'] = $this->order->getOrders();
        if($order_id){
            $data['client'] = $this->order->getOrderById($order_id);
        }
        $data['view'] = 'order/order_form_view';
        $this->load->view('home_view', $data);
    }

    public function view($id = null){
        $data = array();
        $data['products'] = $this->product->getProductsByOrderId($id);
        if(!$id || !$data['products']){
            $this->session->set_flashdata('mensagem', array('danger','Pedido no encontrado!'));
            redirect('/');
        }
        $order_price = 0;
        foreach ($data['products'] as $key => $product){
            $order_price = ($product->preco * $product->product_qtd) + $order_price;
        }
        $data['total'] = $order_price;
        $data['order_id'] = $id;
        $data['view'] = 'order/order_form_view';
        $this->load->view('home_view', $data);
    }

    public function save($id = null)
    {
        $order_products = $this->input->post('product');
        // Se toma el id del cliente
        $client_id = $this->input->post('client_id');
        $sum = 0;
        // Se controla que se haya elegido al menos un producto
        foreach ($order_products as $order_product) {
            $sum +=  $order_product;
        }
        // Si la suma es 0 entonces se redirige al mismo formulario con un mensaje
        if (!$sum) {
            $this->session->set_flashdata('mensagem', array('danger','Seleccione mínimo un producto!'));
            redirect("client/$client_id/");
        }
        $products_ids = array();
        $order_price = 0;

        foreach ($order_products as $key => $product_qtd){
            if($product_qtd){
                $product = $this->product->getProductById($key);
                $order_price = ($product[0]->preco * $product_qtd) + $order_price;
                $products_ids[] = array
                (
                    'product_id' => $key,
                    'product_qtd' => $product_qtd,
                );
            }
        }
        $date = date("Y-m-d H:i:s");
        $form_data = array
        (
            'id' => $id,
            'client_id' => $client_id,
            'data' => $date,
        );
        if(!$id){
            $order_id = $this->order->createOrder($form_data);
            if($order_id){
                foreach ($products_ids as $product_id){
                    $reference_data = array
                    (
                        'order_id' => $order_id,
                        'product_id' => $product_id['product_id'],
                        'product_qtd' => $product_id['product_qtd'],
                    );
                    $send_form = $this->order->makeProductOrderReference($reference_data);
                }
            }
        } else {
            $send_form = $this->order->updateOrder($form_data);
        }

        if($send_form){
            $this->session->set_flashdata('mensagem', array('success','Pedido guardado exitosamente!'));
            redirect('order');
        }
        else
        {
            $this->session->set_flashdata('mensagem', array('danger','Ha ocurrido un error, inténtelo de nuevo.'));
            redirect("client/$client_id/");
        }
    }

    public function delete($id)
    {
        $delete = $this->order->deleteOrder($id);
        if($delete){
            $this->session->set_flashdata('mensagem', array('success','Pedido borrado exitosamente!'));
            redirect('order');
        }
        else
        {
            $this->session->set_flashdata('mensagem', array('danger','Ops! Pedido não encontrado!'));
            redirect('order');
        }
    }
}
