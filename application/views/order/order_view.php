<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container container-person mt-5 p-5">
    <?=write_message()?>
    <h1>Pedidos</h1>
    <table id="product_table" class="table table-striped table-bordered table-responsive-sm" style="width:100%">
        <thead>
        <tr>
            <th>ID</th>
            <th>Cliente</th>
            <th>Fecha</th>
            <th>Acciones</th>
        </tr>
        </thead>
        <tbody>
        <?php
        if($orders) {
            foreach ($orders as $order) { ?>
                <tr>
                    <td><?= $order->id ?></td>
                    <td><?= $order->apellidos . ', ' . $order->nombres ?></td>
                    <td><?php $data = new DateTime($order->data); echo $data->format('d/m/Y - H:i:s') ?></td>
                    <td class="text-center" nowrap>
                        <a class="btn btn-sm btn-primary" href="<?= base_url('order/view/'.$order->id) ?>">Mostrar</a>
                        <a class="btn btn-sm btn-danger delete-order" href="#" data-id="<?= base_url('order/delete/'.$order->id) ?>" data-toggle="modal" data-target="#deleteOrderModal">Borrar</a>

                    </td>
                </tr>
            <?php }
        } else { ?>
            <td class="text-center" colspan="6">No hay pedidos</td>
        <?php } ?>
        </tbody>
    </table>
</div>
<?php $this->load->view('_partials/order/delete_order_confirm_modal') ?>
