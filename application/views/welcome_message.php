<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container container-person mt-5 p-5">
  <?=write_message()?>
  <?php
  $action_form = '/order/save/';
  if(isset($order) && $order){
    foreach ($order as $pedido);
    $action_form = $action_form.$pedido->id ?>
    <h1>Editar Pedido: <?= $pedido->id ?></h1>
  <?php } else { ?>
    <h1>Nuevo Pedido para <?= isset($client) ? $client[0]->apellidos . ', ' . $client[0]->nombres : '' ?></h1>
  <?php }
  if($products) { ?>
    <form id="form_make_order" method="post" action="<?=site_url($action_form)?>">
      <table class="table">
        <thead>
          <th>Nombre</th>
          <th>SKU</th>
          <th>Precio</th>
          <th>Cantidad</th>
        </thead>
        <tbody>
          <?php foreach ($products as $product) { ?>
            <tr>

              <td><?= $product->nome ?></td>
              <td><?= $product->sku ?></td>
              <td>$ <?= $product->preco ?></td>
              <td>
                <input type="number" class="form-control" id="product[<?=$product->id?>]" name="product[<?=$product->id?>]" step="1" min="0" max="100"
                onkeypress='return event.charCode >= 48 && event.charCode <= 57' value="0">
              </td>
            </tr>
          <?php } ?>
        </tbody>
      </table>
      <input type="hidden" name="client_id" id="client_id" value="<?= $client[0]->id ?>">
      <div class="mt-3">
        <button class="btn btn-primary" type="submit">Crear pedido</button>
      </div>
    </form>
  <?php } else { ?>
    <div class="col-sm-12 col-xs-12">No hay productos</div>
  <?php } ?>
</div>
