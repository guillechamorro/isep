<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container container-person mt-5 p-5">
    <?=write_message()?>
    <?php
    $action_form = '/product/save/';
    if(isset($product) && $product){
        foreach ($product as $product);
        $action_form = $action_form.$product->id ?>
        <h1>Editar Producto: <?= $product->nome ?></h1>
    <?php } else { ?>
        <h1>Detalle de Producto</h1>
    <?php } ?>
    <form id="form_product" method="post" enctype="multipart/form-data" action="<?=site_url($action_form)?>">
        <div class="form-row">
            <div class="col-md-4 mb-3">
                <label for="nome">Nombre *</label>
                <input type="text" class="form-control" id="nome" name="nome" placeholder="Nombre" required value="<?= (isset($product) ? $product->nome : '') ?>">
            </div>
            <div class="col-md-4 mb-3">
                <label for="sku">SKU *</label>
                <input type="text" class="form-control" id="sku" name="sku" placeholder="SKU" required value="<?= (isset($product) ? $product->sku : '') ?>">
            </div>
            <div class="col-md-4 mb-3">
                <label for="preco">Precio *</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="inputGroupPrepend">$</span>
                    </div>
                    <input type="number" class="form-control" id="preco" name="preco" placeholder="Precio" min="0" step="0.01" data-number-to-fixed="2" data-number-stepfactor="100" aria-describedby="inputGroupPrepend" required value="<?= (isset($product) ? $product->preco : '') ?>">
                </div>
            </div>
        </div>
        <div class="form-row">
            <div class="col-md-12 mb-3">
                <label for="descricao">Descripción</label>
                <textarea class="form-control" id="descricao" name="descricao" placeholder="Descrição"><?= (isset($product) ? htmlspecialchars($product->descricao) : '') ?></textarea>
            </div>
        </div>
        <button class="btn btn-primary" type="submit">Enviar</button>
        <?= (isset($product) ? '<a href="#" data-id="'.base_url('product/delete/'.$product->id).'" class="btn btn-danger delete-product" data-toggle="modal" data-target="#deleteProductModal">Borrar</a>' : '') ?>
    </form>
</div>
