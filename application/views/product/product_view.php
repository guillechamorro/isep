<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container container-person mt-5 p-5">
    <?=write_message()?>
    <div class="col-md-12 mb-3">
        <div class="row">
            <div class="col-md-6">
                <h1>Productos</h1>

            </div>
            <div class="col-md-6 text-right">

            <a class="btn btn-primary" href="<?= base_url('product/form/') ?>">Nuevo Producto</a>
        </div>

        </div>
    </div>
    <table id="product_table" class="table table-striped table-bordered table-responsive-sm" style="width:100%">
        <thead>
        <tr>
            <th>ID</th>
            <th>Nombre</th>
            <th>SKU</th>
            <th>Precio</th>
            <th>Acciones</th>
        </tr>
        </thead>
        <tbody>
        <?php
        if($products) {
            foreach ($products as $product) { ?>
                <tr>
                    <td><?= $product->id ?></td>
                    <td><?= $product->nome ?></td>
                    <td><?= $product->sku ?></td>
                    <td>$<?= $product->preco ?></td>
                    <td class="text-center" nowrap>
                        <a  class="btn btn-sm btn-success" href="<?= base_url('product/form/'.$product->id) ?>">Editar</a>
                        <a class="btn btn-sm btn-danger delete-product" href="#" data-id="<?= base_url('product/delete/'.$product->id) ?>" data-toggle="modal" data-target="#deleteProductModal">Borrar</a>
                    </td>
                </tr>
            <?php }
        } else { ?>
            <td class="text-center" colspan="6">No hay productos</td>
        <?php } ?>
        </tbody>
    </table>
</div>
<?php $this->load->view('_partials/product/delete_product_confirm_modal') ?>
