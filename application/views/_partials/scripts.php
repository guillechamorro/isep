<!-- jquery -->
<script src="<?= base_url('assets/js/vendor/jquery.min.js') ?>"></script>
<!-- popper -->
<!-- <script src="<?= base_url('assets/js/vendor/popper.min.js') ?>"></script> -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->
<!-- bootstrap -->
<script src="<?= base_url('assets/js/vendor/bootstrap.min.js') ?>"></script>
<!-- datatables -->
<script src="<?= base_url('assets/js/vendor/datatables.min.js') ?>"></script>
<!-- summernote -->
<!-- custom js-->
<script src="<?= base_url('assets/vendor/bootstrap-date-picker/js/bootstrap-datepicker.min.js') ?>"></script>
<script src="<?= base_url('assets/vendor/bootstrap-date-picker/locales/bootstrap-datepicker.es.min.js') ?>"></script>
<!-- <script src="<?= base_url('assets/vendor/sb2admin/vendor/jquery-easing/jquery.easing.min.js') ?>"></script> -->

<!-- Custom scripts for all pages-->
<script src="<?= base_url('assets/vendor/sb2admin/js/sb-admin-2.min.js') ?>"></script>
<script src="<?= base_url('assets/js/custom.js') ?>"></script>
<script src="<?= base_url('assets/js/ajax.js') ?>"></script>
