<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="Guillermo Chamorro">
    <!-- bootstrap -->
    <link rel="stylesheet" href="<?= base_url('assets/css/vendor/bootstrap.min.css') ?>"/>
    <!-- datatable -->
    <link rel="stylesheet" href="<?= base_url('assets/css/vendor/datatables.min.css') ?>"/>

    <!-- summernote -->
    <link rel="stylesheet" href="<?= base_url('assets/css/vendor/datatables.min.css') ?>"/>
    <!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-bs4.css" rel="stylesheet"> -->
    <!-- custom css-->
    <link rel="stylesheet" href="<?= base_url('assets/vendor/bootstrap-date-picker/css/bootstrap-datepicker.min.css') ?>"/>
    <link  rel="icon" href="<?= base_url('assets/img/favicon.png') ?>" type="image/png" />
    <link rel="stylesheet" href="<?= base_url('assets/css/custom.css') ?>"/>
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Frutería La manzana</title>

    <!-- Custom fonts for this template-->
    <link href="<?= base_url('assets/vendor/sb2admin/vendor/fontawesome-free/css/all.min.css') ?>" rel="stylesheet" type="text/css">
    <!-- <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet"> -->

    <!-- Custom styles for this template-->
    <link href="<?= base_url('assets/vendor/sb2admin/css/sb-admin-2.min.css') ?>" rel="stylesheet">

</head>
