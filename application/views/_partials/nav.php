<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

  <!-- Sidebar - Brand -->
  <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?= base_url('/') ?>">
    <div class="sidebar-brand-icon rotate-n-15">
      <i class="fas fa-apple-alt"></i>
    </div>
    <div class="sidebar-brand-text mx-3"><h6>La manzana</h6></div>
  </a>

  <!-- Divider -->
  <hr class="sidebar-divider my-0">

  <!-- Nav Item - Dashboard -->
  <li class="nav-item active">
    <a class="nav-link" href="<?= base_url('/') ?>">
      <i class="fas fa-fw fa-tachometer-alt"></i>
      <span>Inicio</span></a>
  </li>

  <!-- Divider -->
  <hr class="sidebar-divider">

  <!-- Nav Item - Charts -->
  <li class="nav-item">
    <a class="nav-link" href="<?= base_url('product') ?>">
      <i class="fas fa-fw fa-box"></i>
      <span>Productos</span></a>
  </li>

  <!-- Nav Item - Tables -->
  <li class="nav-item">
    <a class="nav-link" href="<?= base_url('client') ?>">
      <i class="fas fa-fw fa-user"></i>
      <span>Clientes</span></a>
  </li>

  <li class="nav-item">
    <a class="nav-link" href="<?= base_url('order') ?>">
      <i class="fas fa-fw fa-asterisk"></i>
      <span>Pedidos</span></a>
  </li>

  <!-- Divider -->
  <hr class="sidebar-divider d-none d-md-block">

  <!-- Sidebar Toggler (Sidebar) -->
  <div class="text-center d-none d-md-inline">
    <button class="rounded-circle border-0" id="sidebarToggle"></button>
  </div>

</ul>
