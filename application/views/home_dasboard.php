<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container container-person mt-5 p-5">
<h1>Inicio</h1>
<p>Para crear nuevos pedidos ingrese a la seccion <a href="<?= base_url('client') ?>">Clientes</a> y bajo un usuario haga click en <a class="bt btn-sm btn-primary" href="#">Crear pedido</a>.
</p>
<p>Ejemplo:</p>
<img src="<?= base_url('/assets/img/crear_pedido.png') ?>" alt="">
</div>
