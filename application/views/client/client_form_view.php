<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
  <div class="container container-person mt-5 p-5">
    <?=write_message()?>
    <?php
    $action_form = '/client/save/';
    if(isset($client) && $client){
      foreach ($client as $client);
      $action_form = $action_form.$client->id ?>
      <h1>Editar Cliente: <?= $client->nombres ?></h1>
    <?php } else { ?>
      <h1>Detalle de Cliente</h1>
    <?php } ?>
    <form id="form_product" method="post" enctype="multipart/form-data" action="<?=site_url($action_form)?>">
      <div class="form-row">
        <div class="col-md-4 mb-3">
          <label for="nombres">Nombres *</label>
          <input type="text" class="form-control" id="nombres" name="nombres" placeholder="Nombre" required
            value="<?= (isset($client) ? $client->nombres : '') ?>">
        </div>
        <div class="col-md-4 mb-3">
          <label for="apellidos">Apellidos *</label>
          <input type="text" class="form-control" id="apellidos" name="apellidos" placeholder="Apellidos" required
            value="<?= (isset($client) ? $client->apellidos : '') ?>">
        </div>
        <div class="col-md-4 mb-3">
          <label for="nombres">DNI *</label>
          <input type="text" class="form-control" id="dni" name="dni" placeholder="DNI" maxlength="8" minlength="7" required
            value="<?= (isset($client) ? $client->dni : '') ?>">
        </div>
        <div class="col-md-4 mb-3">
          <div class="datepicker">
            <label for="apellidos">Fecha de nacimiento</label>
            <input type="text" class="form-control" id="fecha_nac" name="fecha_nac" placeholder="Fecha de nacimiento"
                value="<?php
                $fecha = isset($client) ? new DateTime($client->fecha_nac) : false;
                echo $fecha ? $fecha->format('d/m/Y') : '';
                ?>">
          </div>
        </div>
        <div class="col-md-4 mb-3">
          <label for="nombres">Provincia</label>
          <select class="form-control" id="provincia_id" name="provincia_id">
            <option value=""></option>
            <?php if (isset($provincias)): ?>
            <?php foreach ($provincias as $provincia): ?>
                <?php // Se selecciona la provincia relacionada al cliente en editar ?>
              <?php if ($client->provincia_id && $client->provincia_id === $provincia->id): ?>
                <option value="<?= $provincia->id ?>" selected><?= $provincia->provincia ?></option>
              <?php else: ?>
                <option value="<?= $provincia->id ?>"><?= $provincia->provincia ?></option>
              <?php endif; ?>
            <?php endforeach; ?>
            <?php endif; ?>
          </select>
        </div>
        <div class="col-md-4 mb-3">
          <label for="apellidos">Email</label>
          <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="<?= (isset($client) ? $client->email : '') ?>">
        </div>
      </div>
      <button class="btn btn-primary" type="submit">Enviar</button>
      <?= (isset($client) ? '<a href="#" data-id="'.base_url('client/delete/'.$client->id).'" class="btn btn-danger delete-product" data-toggle="modal" data-target="#deleteClientModal">Borrar</a>' : '') ?>
    </form>
  </div>
