<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container container-person mt-5 p-5">
    <?=write_message()?>
    <div class="col-md-12 mb-3">
        <div class="row">
            <div class="col-md-6">
                <h1>Clientes</h1>
            </div>

            <div class="col-md-6 text-right">
                <a class="btn btn-primary" href="<?= base_url('client/form/') ?>">Nuevo Cliente</a>
            </div>

        </div>
    </div>
    <table id="client_table" class="table table-striped table-bordered table-responsive-sm" style="width:100%">
        <thead>
        <tr>
            <th>Id</th>
            <th>Nombres</th>
            <th>Apellidos</th>
            <th>DNI</th>
            <th>Fecha de nacimiento</th>
            <th>Email</th>
            <th>Provincia</th>
            <th>Acciones</th>
        </tr>
        </thead>
        <tbody>
        <?php
        if($clients) {
            foreach ($clients as $client) { ?>
                <tr>
                    <td><?= $client->id ?></td>
                    <td><?= $client->nombres ?></td>
                    <td><?= $client->apellidos ?></td>
                    <td><?= $client->dni ?></td>
                    <td>
                        <?php // Conversión de fecha de Y-m-d a d-m-Y ?>
                        <?php $fecha = $client->fecha_nac ? new DateTime($client->fecha_nac) : false; ?>
                        <?= $fecha = $fecha ? $fecha->format('d/m/Y') : '<i>No definida</i>' ?>

                    </td>
                    <td><?= $client->email ?></td>
                    <td><?= $client->provincia_id ?></td>
                    <td class="text-center" nowrap>
                            <a class="btn btn-sm btn-success" href="<?= base_url('client/form/'.$client->id) ?>">Editar</a>
                            <a class="btn btn-sm btn-primary create-order" href="<?= base_url('/client/' . $client->id) ?>">Crear pedido</a>
                    </td>
                </tr>
            <?php }
        } else { ?>
            <td class="text-center" colspan="6">No existen clientes</td>
        <?php } ?>
        </tbody>
    </table>
</div>
